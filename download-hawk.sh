#!/bin/bash

# Build script for Hawk Server NoGPL Docker Image
#
# This script scrapes the Eclipse website for the latest server version,
# and downloads it to a predefined filename.

set -e

if test -z "$HAWK_VERSION"; then
  echo "You need to define HAWK_VERSION before running this plugin."
  echo "For example: HAWK_VERSION=2.3.0 $0"
  exit 1
fi

# Scrape the Eclipse website
artifact_url=$(curl -s https://download.eclipse.org/hawk/${HAWK_VERSION}/ | xmllint --html --xpath 'string(//a[contains(string(@href), "hawk-server-nogpl_") and contains(string(@href), "linux")]/@href)' --format - 2>/dev/null)

# Download the server ZIP (the '&r=1' is to download the file from best mirror directly)
echo "Downloading Hawk $HAWK_VERSION from $artifact_url"
wget -O hawk-server.zip "$artifact_url&r=1"
