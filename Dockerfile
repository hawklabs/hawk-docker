FROM alpine AS download

WORKDIR /tmp

ARG HAWK_VERSION=2.2.0
RUN apk add --no-cache bash curl libxml2-utils
COPY download-hawk.sh .

RUN /tmp/download-hawk.sh \
    && unzip hawk-server.zip \
    && mv hawk-server-nogpl_* hawk-server \
    && rm hawk-server.zip

FROM adoptopenjdk:11-jre-hotspot

RUN adduser --disabled-password --gecos '' hawk
COPY --from=download --chown=hawk /tmp/hawk-server /home/hawk/server
COPY entrypoint.sh /usr/local/bin

WORKDIR /home/hawk/server
USER hawk

# Use subfolder to store keyring + pass (for named volumes)
#
# Create workspace folder as well (needed for volume mountpoint)
RUN mkdir keyring workspace

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["/home/hawk/server/hawk-server"]
