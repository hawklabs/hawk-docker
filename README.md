# hawk-docker

This repository provides a Docker image for Eclipse Hawk (https://www.eclipse.org/hawk/).
It uses the official server products, as built by the CI in Eclipse Hawk.

## Using with Docker Compose

The [example-gradle](./example-gradle) folder contains a sample Gradle project with a `docker-compose.yml` file that uses the latest build of the Docker image.
Consult the [README](example-gradle/README.md) in that folder for details.

## Building with plain Docker

You can build the image with plain Docker, like this:

```sh
docker build -t hawk:latest .
```

If you want to build against a specific version of Hawk, you can provide the `HAWK_VERSION` build argument. For instance, to build a 2.2.0 server image:

```sh
docker build --build-arg HAWK_VERSION=2.2.0 -t hawk:v2.2.0 .
```