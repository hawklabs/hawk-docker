# Sample Gradle project for Eclipse Hawk Docker images

This is an example project using [Docker Compose](https://docs.docker.com/compose/) and [Gradle](https://gradle.org/) to automate the use of a Hawk server.

Compose manages the Docker container created from the Hawk Docker image, and Gradle uses the Java Thrift API bindings to create an index instance within the server, register metamodels, and ask the server to index the [GraBaTs'09 set0 model](https://web.imt-atlantique.fr/x-info/atlanmod/index.php?title=GraBaTs_2009_Case_Study) inside the `models` directory.
There are several tasks to run simple queries in the indexed models.

## Prerequisites

Install Docker Compose following the [official instructions](https://docs.docker.com/compose/install/).

This document assumes you can run `docker compose` without `sudo`.

## Step 1: start the server

Start the server with:

```shell script
docker compose up -d
```

This will create and start a container from the latest Eclipse Hawk Docker image built in Gitlab.
Keep in mind that if you wish to update the image after this first download, you will need to pull and update the container.
Run the following command in that case (existing data will be preserved through the use of Docker volumes):

```shell script
docker compose pull
docker compose up -d
```

## Step 2: index the models

Use the Gradle `build.gradle` file to create a Hawk `hawk-set0` instance inside the server using the Greycat backend, register the JDTAST metamodel, and add the `models` directory as a model repository to be indexed and monitored:

```shell script
./gradlew indexModels
```

Hawk may take some time to index the models.
You can follow its progress through the logs:

```shell script
docker compose logs -f
```

You can exit those logs at any time with Ctrl+C.
Note that you can re-run `./gradlew indexModels` at any time: if the models are already indexed, it will only make sure that the Hawk `hawk-set0` instance is running.

## Step 3: query the models

### Counting all objects

To print out the number of objects inside all indexed models, run this command:

```shell script
./gradlew countInstances
```

The output will look like this:

```shell script
Query ran in 343ms. Result is:
<QueryResult vInteger:69680>
```

Hawk keeps track of the query execution time within the server, excluding networking and serialisation overheads.
Query results are wrapped in a typesafe way using the Thrift API elements: `vInteger` shows the result is an integer number.
We have 69680 objects in the sample `set0` model.

### Counting all type declarations

To print out the number of `TypeDeclaration`s in the model, run this command:

```shell script
./gradlew countTypeDeclarations
```

You will get a result like this:

```shell script
> Task :countTypeDeclarations
Query ran in 14ms. Result is:
<QueryResult vInteger:14>
```

This query is much faster: Hawk has direct access to all the instances of a type.
Hawk does not need to iterate through the whole model.

### Using the experimental web app

The experimental [hawk-react-ui](https://gitlab.com/hawklabs/hawk-react-ui) is available from http://localhost:8081.
You will need to have indexed some models first and started the Hawk instance first (the current version cannot manage instances yet).

## Step 4: stop the server

There are two options for stopping the server.
You can stop it while keeping the container, like this:

```shell script
docker compose stop
```

Alternatively, you can stop the server and delete the container to save some disk space:

```
docker compose down
```

Both options preserve the indexed models through the use of Docker volumes.
The only difference is whether the container will be reused or recreated the next time you start Hawk.
