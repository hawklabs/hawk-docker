#!/bin/bash

INI=hawk-server.ini
KEYRING=keyring/mondo.keyring
PASSWORD=keyring/mondo.pass

# Length of the random password file, in bytes (before base64 encoding)
PASSLENGTH=100

# Generate a random password file if it does not exist
if ! test -f "$KEYRING"; then
    touch "$KEYRING"
    touch "$PASSWORD"
    chmod 600 "$KEYRING" "$PASSWORD"
    head -c "$PASSLENGTH" /dev/random | base64 > "$PASSWORD"
    chmod 400 "$PASSWORD"
fi

# Ensure the keyring in the volume is used
if ! grep -q eclipse.keyring "$INI"; then
    sed -i -e "/[-]vmargs/i -eclipse.keyring\n$KEYRING\n-eclipse.password\n$PASSWORD" "$INI"
fi

# Ensure that listenAll is set to 'true' (needed to have Artemis connectivity)
if ! grep -q -e "-Dhawk.artemis.listenAll=true" "$INI"; then
    sed -i -e "/hawk.artemis.listenAll/s/false/true/" "$INI"
fi

exec "$@"
